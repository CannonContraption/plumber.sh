#!/bin/sh

#####  #      #    # #    # #####  ###### #####       ####  #    #
#    # #      #    # ##  ## #    # #      #    #     #      #    #
#    # #      #    # # ## # #####  #####  #    #      ####  ######
#####  #      #    # #    # #    # #      #####  ###      # #    #
#      #      #    # #    # #    # #      #   #  ### #    # #    #
#      ######  ####  #    # #####  ###### #    # ###  ####  #    #

# Designed to plumb the primary selection in X into one of a variety of
# different programs and actions.
#
# Designed to work from any application that supports primary selection in X
# (e.g. terminal emulators, email programs, Firefox, etc...)

################################################################################
# Common Settings
#-------------------------------------------------------------------------------

# File containing all of your git workspaces for searching
# Should be tab separated, so name\tfull path
workspacesfile="$HOME/.workspaces"
# Program for menus; should be dmenu-like (e.g. rofi -dmenu, just dmenu, etc)
menuprogram="rofi -dmenu"
# Error message executable. Could be xmessage, or rofi -e, or similar
errorprogram="rofi -e"
# Your preferred text editor. Should accept +n syntax to go to line n.
preferrededitor="emacsclient -c"
# I set an env var called $browser, but use whatever you like to determine this:
preferredbrowser=$WWWBROWSER
# Your preferred PDF reader.
preferredpdfread="mupdf"
# Your preferred Email client. Should accept address as first argument
preferredsmtpclient="sylpheed"

################################################################################
# Internal Setup
#-------------------------------------------------------------------------------

# Grab the selection off primary selection
selection=$(xsel -p)

################################################################################
# Mode functions
#-------------------------------------------------------------------------------

# grep_workspaces
#
# Takes output from grep -n and then launches an editor at the line specified
# by grep, in the file specified by grep.
grep_workspaces()
{
    # First ask for a workspace. We need a list of these since grep does not
    # output absolute paths, and the primary selection gives no context.
    #
    # If you work on a greppable thing a lot, it makes sense to put its relative
    # working directory in the workspaces file, then only grep from there.
    # Unfortunately I can't think of a simple workaround right this instant...
    workspace=$(cat $workspacesfile | awk -F '	' '{print $1}' | $menuprogram)
    # Get the path from the workspace name
    workspacepath=$(cat $workspacesfile | awk -F '	' '{if ($1 == "'$workspace'") print $2}')
    # Launch an editor at the specified line.
    $preferrededitor $workspacepath$(echo "$selection" | awk -F ':' '{print $1}') \+$(echo $selection | awk -F ':' '{print $2}')
}

# add_task
#
# Adds a task to the taskwarrior inbox tag
add_task()
{
	task +inbox add $selection
}

# view_webpage
#
# Launches a web browser and sends it to the selected URL
view_webpage()
{
    $preferredbrowser "$selection"
}

# send_email
#
# Opens your preferred email client and starts up a new 'compose message' window
# with the address in the primary selection as the recipient.
send_email()
{
    $preferredsmtpclient "$selection"
}

# pdf_read
#
# Opens the PDF reader you set, and points it at the file in the primary
# selection. Should either be relative to ~ or an absolute path.
pdf_read()
{
    mupdf "$selection"
}

################################################################################
# Program Execution
#-------------------------------------------------------------------------------

# Automatic desired action selection...
#-------------------------------------------------------------------------------

# Email address
echo "$selection" | grep -E '.+@.+\..+' > /dev/null 2>&1
if [ $? = 0 ]; then
    send_email
    exit 0;
fi

# Grep line
echo "$selection" | grep -E '.+:[0-9]+:.+' > /dev/null 2>&1
if [ $? = 0 ]; then
    grep_workspaces
    exit 0;
fi

# Website
echo "$selection" | grep -E '$http[s]{0,1}://' > /dev/null 2>&1
if [ $? = 0 ]; then
    view_webpage
    exit 0;
fi

# Didn't figure it out ourselves, fall back to asking the user
#-------------------------------------------------------------------------------

# Available actions - add to this list and to the case statement below
actionslist="task\ngrep-workspaces\nweb\nemail\npdfread"

desiredaction=$(echo $actionslist | $menuprogram)

case $desiredaction in
    grep-workspaces ) grep_workspaces ;;
    email           ) send_email ;;
    web             ) view_webpage ;;
    pdfread         ) pdf_read ;;
	task            ) add_task ;;
    * ) $errorprogram "Action $desiredaction not implemented or not supported." ;;
esac
